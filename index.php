<?php
    $servername = "remotemysql.com";
    $username = "lL4CBga8AJ";
    $password = "cWuHOhXq19";
    $dbname = "lL4CBga8AJ";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get temperature
    function getTemperature() {
        global $conn;
        $sql = "SELECT temperature FROM temp";
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()) {
            return $row["temperature"];
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Very Simple DS18b20 App</title>
</head>
<body>
    <h1>Very Simple DS18b20 App</h1>
    <h2><?php print getTemperature(); ?>°</h2>
</body>
</html>